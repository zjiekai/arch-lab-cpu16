library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity cpu16 is
  port ( 
    iow, ior, mrd, mwr: out std_logic;
    run, clk: in std_logic;
    prix, krix: in std_logic;
    mclk: inout std_logic;
    ab: inout std_logic_vector(15 downto 0);
    db: inout std_logic_vector(15 downto 0)
  );
end cpu16;

architecture Jiekai of cpu16 is

  signal t0, t1, t2: std_logic;
  signal LDR, STR, MOVL, MOVH, ADD, SUB, BJ, BC, BK, BP: std_logic;

  signal crd, crdx: std_logic;

  signal jmp, cond: std_logic;
  signal wpc, ire: std_logic;
  signal sal: std_logic_vector(2 downto 0);
  signal sma, smb, wrr: std_logic_vector(3 downto 0);

  signal imm13, IR, aluout, ra, rb, r0, r1, r2, r3, r4, r5, r6, r7, pc, pc_input: std_logic_vector(15 downto 0);


begin

  crdx <= '0' when (t0 = '1') else '1';
  crd <= crdx or not mclk;
  mrd <= crd or ab(15);
  
  process(clk, run)
  begin
    if (run = '0') then
      mclk <= '0';
    elsif (clk'event and clk = '0') then
      mclk <= not mclk;
    end if;
  end process;

  process(mclk, run, t2)
  begin
    if (run = '0') then
      t0 <= '0'; t1 <= '0'; t2 <= '1';
    elsif (mclk'event and mclk = '0') then
      if (t2 = '1') then
        t0 <= '1'; t1 <= '0'; t2 <= '0';
      else
        t0 <= '0'; t1 <= t0;  t2 <= t1;
      end if;
    end if;
  end process;

  LDR  <= '1' when IR(15 downto 12) = "0010" else '0';
  STR  <= '1' when IR(15 downto 12) = "0011" else '0';
  MOVL <= '1' when IR(15 downto 11) = "01100" else '0';
  MOVH <= '1' when IR(15 downto 11) = "01101" else '0';
  ADD  <= '1' when IR(15 downto  9) = "0000010" else '0';
  SUB  <= '1' when IR(15 downto  9) = "0000011" else '0';
  BJ   <= '1' when IR(15 downto 12) = "1100" else '0';
  BC   <= '1' when IR(15 downto 12) = "1101" else '0';
  BK   <= '1' when IR(15 downto 12) = "1110" else '0';
  BP   <= '1' when IR(15 downto 12) = "1111" else '0';

  process(mclk, run)
  begin
    if (run = '0') then
      pc <= "0000000000000000";
    elsif(mclk'event and mclk = '0') then

      if (wpc = '1') then
        pc <= pc_input;
      end if;

      if (ire = '1') then
        IR <= db;
      end if;

      case wrr is
        when "0000" => r0 <= aluout;
        when "0001" => r1 <= aluout;
        when "0010" => r2 <= aluout;
        when "0011" => r3 <= aluout;
        when "0100" => r4 <= aluout;
        when "0101" => r5 <= aluout;
        when "0110" => r6 <= aluout;
        when "0111" => r7 <= aluout;
        when others => r7 <= r7;
      end case;
      
      if (t1 = '1' and (ADD or SUB) = '1') then 
        cond <= aluout(15);
      end if;

    end if;
  end process;

  ra <= r0 when sma = "0000" else
        r1 when sma = "0001" else
        r2 when sma = "0010" else
        r3 when sma = "0011" else
        r4 when sma = "0100" else
        r5 when sma = "0101" else
        r6 when sma = "0110" else
        r7 when sma = "0111" else
        "1111111111111111";

  rb <= r0 when smb = "0000" else
        r1 when smb = "0001" else
        r2 when smb = "0010" else
        r3 when smb = "0011" else
        r4 when smb = "0100" else
        r5 when smb = "0101" else
        r6 when smb = "0110" else
        r7 when smb = "0111" else
        IR when smb = "1010" else
        "1111111111111111";

  aluout <= ra + rb when sal = "000" else
            ra - rb when sal = "001" else
            ra(15 downto 8) & rb(7 downto 0) when sal = "010" else
            rb( 7 downto 0) & ra(7 downto 0) when sal = "011" else
            pc + imm13 when sal = "100" else
            "0000000000000000";

  process(IR)
  begin
    imm13 <= (others => IR(12));
    imm13(13 downto 1) <= IR(12 downto 0);
    imm13(0) <= '0';
  end process;

  ab <= pc;
  db <= "ZZZZZZZZZZZZZZZZ";

  ire <= '1' when (t0 = '1') else '0';
  jmp <= '1' when (t1 = '1') and ((BJ = '1') or (BC = '1' and cond = '1') or (BK = '1' and krix = '0') or (BP = '1' and prix = '0')) else '0';

  wpc <= '1' when (t0 = '1') or (jmp = '1') else '0';
  wrr <= '0'&IR(2  downto 0) when (t1 = '1' and (ADD  or SUB ) = '1') else
         '0'&IR(10 downto 8) when (t1 = '1' and (MOVL or MOVH) = '1') else
         "1111";
  sma <= '0'&IR(5  downto 3) when (t1 = '1' and (ADD  or SUB ) = '1') else
         '0'&IR(10 downto 8) when (t1 = '1' and (MOVL or MOVH) = '1') else
         "1111";
  smb <= '0'&IR(8  downto 6) when (t1 = '1' and (ADD  or SUB ) = '1') else
         "1010" when (t1 = '1' and (MOVL or MOVH) = '1') else
         "1111";
  sal <= "000" when (t1 = '1' and ADD  = '1') else
         "001" when (t1 = '1' and SUB  = '1') else
         "010" when (t1 = '1' and MOVL = '1') else
         "011" when (t1 = '1' and MOVH = '1') else
         "100" when (t1 = '1' and jmp  = '1') else
         "111";

  pc_input <= aluout when (jmp = '1') else pc + 2;

end Jiekai;
