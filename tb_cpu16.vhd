library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_cpu16 is 
end tb_cpu16;

architecture Jiekai of tb_cpu16 is

  component cpu16
    port (
      iow, ior, mrd, mwr: out std_logic;
      prix, krix: in std_logic;
      run, clk: in std_logic;
      mclk: inout std_logic;
      ab: inout std_logic_vector(15 downto 0);
      db: inout std_logic_vector(15 downto 0)
    );
  end component;

  signal iow, ior, mrd, mwr: std_logic;
  signal run, clk, mclk: std_logic;

  signal ab: std_logic_vector(15 downto 0);
  signal db: std_logic_vector(15 downto 0);

  constant clk_period: time := 200 ns;

  type memory_type is array(0 to 31) of std_logic_vector(15 downto 0);
  signal ram: memory_type;

begin

  uut: cpu16 port map (
    iow => iow, ior => ior, mrd => mrd, mwr => mwr,
    run => run, clk => clk, mclk => mclk,
    prix => '0', krix => '0',
    ab => ab, db => db
  );

  run <= '0', '1' after clk_period*2;

  process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process;

  db <= ram(to_integer(unsigned(ab(5 downto 1))))
        when mrd = '0' else "ZZZZZZZZZZZZZZZZ";

  process(run)
  begin
    if (run'event and run = '1') then
      -- MOVL R0, 10101010
      -- ram(0) <= "0110000010101010";
      -- MOVH R0, 10101010
      -- ram(1) <= "0110100010101010";
      -- MOVL R0, 00001111
      -- MOVH R0, 00000001
      ram(0) <= "0110000000001111";
      ram(1) <= "0110100000000001";
      -- MOVL R1, 00001111
      -- MOVH R1, 00000010
      ram(2) <= "0110000100001111";
      ram(3) <= "0110100100000010";
      -- ADD 
      --ram(4) <= "0000010001000011";
      -- SUB
      ram(4) <= "0000011001000010";
      ram(5) <= "1101111111111010";
    end if;
  end process;
end;
